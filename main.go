package main

import (
	"github.com/spf13/cobra"
	"bitbucket.org/kindlychung/chardatagen/SVGGeneration"
)

func main() {

	var outputDir = "";
	var centerRatio = 0.5;
	var nUnique int = 0

	var cmdTwoChar = &cobra.Command{
		Use:   "twoChar",
		Short: "Generate 224x224 images with two characters in it",
		Long:  ``,
		Args:  cobra.MinimumNArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			// args are positional args, which are not needed in this app
			//fmt.Println("Print: " + strings.Join(args, " "))
			SVGGeneration.TwoCharData(outputDir, centerRatio, nUnique)
		},
	}

	cmdTwoChar.Flags().StringVarP(&outputDir, "outputDir", "o", "/tmp", "Output directory for images")
	cmdTwoChar.Flags().Float64VarP(&centerRatio, "centerRatio", "c", .5, "Position of the boudary between the two characters")
	cmdTwoChar.Flags().IntVarP(&nUnique, "nUnique", "n", 1, "Number of unique character combinations")

	var rootCmd = &cobra.Command{Use: "charDataGen"}
	rootCmd.AddCommand(cmdTwoChar)
	rootCmd.Execute()
}
