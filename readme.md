# Generate images of characters for training text recognition models


## Install

Get the executable by:
```
go build
```

## Usage

Type `./charDataGen -h` to see the help message.


