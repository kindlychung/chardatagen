package SVGGeneration

import (
	"math/rand"
	"os"
	"time"
	"fmt"
	"github.com/ajstarks/svgo"
	"io/ioutil"
	"path"
	"image"
	"image/jpeg"
	"bytes"
	"encoding/base64"
	"github.com/satori/go.uuid"
	"os/exec"
	"github.com/anthonynsimon/bild/imgio"
	"github.com/anthonynsimon/bild/effect"
	"github.com/anthonynsimon/bild/transform"
	"github.com/anthonynsimon/bild/adjust"
	"github.com/anthonynsimon/bild/blur"
	"github.com/anthonynsimon/bild/noise"
	"github.com/anthonynsimon/bild/blend"
	"path/filepath"
)

func randColor() string {
	r := rand.Intn(256)
	g := rand.Intn(256)
	b := rand.Intn(256)
	return fmt.Sprintf("rgb(%d,%d,%d)", r, g, b)
}

func randFontSize(min int, max int) int {
	s := rand.Intn(max-min) + min
	return s
}

type FontPool struct {
	fonts []string
}

func NewFontPool(fonts []string) FontPool {
	return FontPool{fonts}
}

func (randFonts *FontPool) randFont() string {
	length := len(randFonts.fonts)
	n := rand.Intn(length)
	return randFonts.fonts[n]
}

type BackgroundPool struct {
	imageDir   string
	imageFiles []os.FileInfo
}

func NewBackgroundPool(dir string) BackgroundPool {
	info, err := ioutil.ReadDir(dir)
	if err != nil {
		fmt.Printf("Failed to read directory: %s", dir)
		os.Exit(1)
	}
	return BackgroundPool{dir, info}
}

func (bg *BackgroundPool) randBackground() string {
	length := len(bg.imageFiles)
	n := rand.Intn(length)
	return path.Join(bg.imageDir, bg.imageFiles[n].Name())
}

func (bg *BackgroundPool) randBackgroundBase64() string {
	imgFile := bg.randBackground()
	return jpgToBase64(imgFile)
}

func jpgToBase64(jpgFile string) string {
	f, err := os.Open(jpgFile)
	if err != nil {
		fmt.Printf("Failed to open %s", jpgFile)
		os.Exit(1)
	}
	defer f.Close()
	imgData, _, err := image.Decode(f)
	if err != nil {
		fmt.Printf("Failed to decode %s", jpgFile)
		os.Exit(1)
	}
	f.Seek(0, 0)
	var buff bytes.Buffer
	options := jpeg.Options{90}
	jpeg.Encode(&buff, imgData, &options)
	encodedString := base64.StdEncoding.EncodeToString(buff.Bytes())
	return "data:image/png;base64," + encodedString
}

type TextSvg struct {
	width       int
	height      int
	backgrounds BackgroundPool
}

func NewTextSvg(width int, height int, backgrounds BackgroundPool) TextSvg {
	return TextSvg{
		width,
		height,
		backgrounds,
	}
}

func (textSvg *TextSvg) generate(filename string, fontSize int, fontFamily string, runes []rune, xPosition int) {
	f, err := os.Create(filename)
	if err != nil {
		fmt.Printf(`Failed to create file: %s`, filename)
	}
	defer f.Close()
	canvas := svg.New(f)
	canvas.Start(textSvg.width, textSvg.height)
	canvas.Rect(0, 0, textSvg.width, textSvg.height, fmt.Sprintf("fill:%s", randColor()))
	//canvas.Image(0, 0, textSvg.width, textSvg.height, textSvg.backgrounds.randBackgroundBase64())
	style := fmt.Sprintf("font-family:'%s';font-size:%dpx;fill:%s", fontFamily, fontSize, randColor())
	y := wiggle(textSvg.height/2+fontSize/3, 10)
	if len(runes) == 1 {
		canvas.Text(xPosition, y, string(runes), style+";text-anchor:middle")
	} else if len(runes) == 2 {
		canvas.Text(xPosition, y, string(runes[0]), style+";text-anchor:end")
		canvas.Text(xPosition, y, string(runes[1]), style+";text-anchor:start")
	}
	canvas.End()
}

type TextPool struct {
	chars []int32
}

func NewTextPool(chars []rune) TextPool {
	return TextPool{chars}
}

func (rt *TextPool) generate(n int) []rune {
	length := len(rt.chars)
	var buffer bytes.Buffer
	for i := 0; i < n; i++ {
		idx := rand.Intn(length)
		buffer.WriteRune(rt.chars[idx])
	}
	return []rune(buffer.String())
}

type TextSvgConfg struct {
	backgroundPool BackgroundPool
	fontPool       FontPool
	textPool       TextPool
	outputDir      string
}

func DefaultTextSvgConfig(outputDir string) TextSvgConfg {
	bgDir := "background"
	backgroundPool := NewBackgroundPool(bgDir)
	fontPool := NewFontPool([]string{
		"DejaVu Sans",
		"DejaVu Serif",
		"Impact",
		"courier",
		"monospace",
	})
	textPool := NewTextPool([]rune("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"))
	return TextSvgConfg{
		backgroundPool: backgroundPool,
		fontPool:       fontPool,
		textPool:       textPool,
		outputDir:      outputDir,
	}
}

func execCommand(command string, args []string) {
	cmd := exec.Command(command, args...)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println(fmt.Sprint(err) + ": " + stderr.String())
		return
	}
	fmt.Println("Result: " + out.String())
}

func wiggle(orig int, wiggleRange int) int {
	n := rand.Intn(wiggleRange*2) - wiggleRange
	return orig + n
}
func wiggleFloat64(orig float64, wiggleRange float64) float64 {
	r := rand.Float64() * wiggleRange
	return orig + r
}

func transformImage(imgFile string, rotationLimit int, brightnessLimit float64, contrastLimit float64, hueLimit int, saturationLimit float64, blurLimit float64, ) {
	img, err := imgio.Open(imgFile)
	if err != nil {
		panic(err)
	}
	imgSize := img.Bounds().Size()
	width := imgSize.X
	height := imgSize.Y
	blurRadius := wiggleFloat64(blurLimit/2.0, blurLimit/2.0)
	brightnessChange := wiggleFloat64(0.0, brightnessLimit)
	contrastChange := wiggleFloat64(0.0, contrastLimit)
	//saturationChange := wiggleFloat64(0.0, saturationLimit)
	//hueChange := wiggle(0, hueLimit)
	uniformNoise := noise.Generate(width, height, &noise.Options{Monochrome: false, NoiseFn: noise.Uniform})
	gaussianNoise := noise.Generate(width, height, &noise.Options{Monochrome: false, NoiseFn: noise.Gaussian})
	var noise image.RGBA
	if rand.Float64() > .5 {
		noise = *uniformNoise
	} else {
		noise = *gaussianNoise
	}
	var imageArray = []image.Image{
		//effect.Invert(img),
		transform.Rotate(img, float64(wiggle(0, rotationLimit)), nil),
		//transform.Rotate(img, float64(wiggle(0, rotationLimit)), nil),
		//transform.Rotate(img, float64(wiggle(0, rotationLimit)), nil),
		//blur.Box(img, blurRadius),
		blur.Gaussian(img, blurRadius),
		adjust.Brightness(img, brightnessChange), // from -1 to 1
		adjust.Contrast(img, contrastChange),     // from -1 to 1
		//adjust.Saturation(img, saturationChange), // from -1 to 1
		//adjust.Hue(img, hueChange),               // from -360 to 360
		effect.Grayscale(img),
		//blend.Multiply(img, &noise),
		//blend.Subtract(img, &noise),
		blend.Add(img, &noise),
	}
	ext := filepath.Ext(imgFile)
	var imgType imgio.Format
	if ext == ".png" {
		imgType = imgio.PNG
	} else if ext == ".jpg" || ext == ".jpeg" {
		imgType = imgio.JPEG
	} else {
		panic("Unknown image type. Only png and jpg supported.")
	}
	fileRoot := imgFile[0:len(imgFile)-len(ext)]
	for index, imgNew := range imageArray {
		outputFile := fmt.Sprintf("%s_%d%s", fileRoot, index, ext)
		if err := imgio.Save(outputFile, imgNew, imgType); err != nil {
			panic(err)
		}
	}
}

func outputPath(dir, fontFamily string, fontSize int, text string, xPos int, id uuid.UUID, ext string, ) string {
	return path.Join(dir, fmt.Sprintf("%s-%d-%s-%d-%s%s", fontFamily, fontSize, string(text), xPos, id, ext))
}

func TwoCharData(folder string, centerRatio float64, nImg int) {
	os.MkdirAll(folder, os.ModePerm);
	rand.Seed(time.Now().Unix())
	splittedConfig := DefaultTextSvgConfig(folder)
	textSvg := NewTextSvg(224, 224, splittedConfig.backgroundPool)
	for i := 0; i < nImg; i++ {
		fontSize := randFontSize(220, 270)
		fontFamily := splittedConfig.fontPool.randFont()
		text := splittedConfig.textPool.generate(2)
		id := uuid.NewV4()
		xPos := int(float64(textSvg.width) * centerRatio)
		outputSVG := outputPath(splittedConfig.outputDir, fontFamily, fontSize, string(text), xPos, id, ".svg")
		outputPNG := outputPath(splittedConfig.outputDir, fontFamily, fontSize, string(text), xPos, id, ".png")
		textSvg.generate(outputSVG, fontSize, fontFamily, text, wiggle(xPos, 10))
		execCommand("inkscape", []string{
			"-z", "-e", outputPNG, "-w", fmt.Sprintf("%d", textSvg.width), "-h", fmt.Sprintf("%d", textSvg.height), outputSVG,
		})
		os.Remove(outputSVG)
		transformImage(
			outputPNG,
			5,
			0.5, // [-1, 1]
			0.5, // [-1, 1]
			100, // [-360, 360]
			0.5, // [-1, 1]
			20,  // [0, min(width, height)]
		)
	}
}

